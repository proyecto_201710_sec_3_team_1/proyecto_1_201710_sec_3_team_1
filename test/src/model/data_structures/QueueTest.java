package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;

public class QueueTest {

	private Queue<String> queue;

	private void init(){
		queue = new Queue<String>();
		
	}
	
	private void queueItems(){
		int i = 0;
		while(i<15){
			String add = "cadena " + i;
			queue.enQueue(add);
			//System.out.println("agregado " + add);
			i++;
		}
		
	}
	
	@Test
	public void testQueue() {
		init();
		queueItems();
	}

	@Test
	public void testDeQueue() {
		init();
		queueItems();
		
		System.out.println(queue.deQueue());
		System.out.println(queue.deQueue());
		System.out.println(queue.deQueue());
	}

	@Test
	public void testIsEmpty() {
		init();
		System.out.println(queue.isEmpty());
		queueItems();
		System.out.println(queue.isEmpty());
	}

	@Test
	public void testSize() {
		init();
		System.out.println(queue.size());
		queueItems();
		System.out.println(queue.size());		
	}

}
