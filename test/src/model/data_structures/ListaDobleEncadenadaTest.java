package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

public class ListaDobleEncadenadaTest {

	private ListaDobleEncadenada<String> a;
	
	public void setup1(){
		a = new ListaDobleEncadenada<String>();
		String cadena = "CADENA ";
		int contador = 0;
		
		while (contador<15){
			a.agregarElementoFinal(cadena + contador);
			contador++;
		}

	}
	
	@Test
	public void testAgregarElementoFinal() {
		setup1();
		
		int contador = 0;
		while (contador < 15)
		assertEquals("CADENA "+ contador, a.darElemento(contador++));
				
	}

	@Test
	public void testDarElemento() {
		setup1();
		
		assertEquals("CADENA 2", a.darElemento(2));
		
		assertEquals("CADENA 5", a.darElemento(5));
		
		assertEquals("CADENA 11", a.darElemento(11));
	}

	@Test
	public void testDarNumeroElementos() {
		setup1();
		
		assertEquals(15,a.darNumeroElementos());
	}

	@Test
	public void testEliminarElemento() {
		setup1();
		
		String eliminado = a.eliminarElemento(4);
		
		assertNotEquals(eliminado, a.darElemento(4));
	}

}
