package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;

public class StackTest {

	private Stack<String> stack;

	public void init(){
		stack = new Stack<String>();
	}
	
	public void pusher(){
		
		int i = 0;
		
		while(i<15){
			String add = "Cadena" + i;
			stack.push(add);
			
			i++;
		}
	}

	@Test
	public void testPush() {
		
		init();
		pusher();
	}

	@Test
	public void testPop() {
		
		init();
		pusher();
		System.out.println("popped " +stack.pop());
		
	}

	@Test
	public void testIsEmpty() {
		init();
		
		System.out.println(stack.isEmpty());
		
		pusher();
		
		System.out.println(stack.isEmpty());
	}

	@Test
	public void testSize() {
		init();
		System.out.println(stack.size());
		
		pusher();
		
		System.out.println(stack.size());
	}
}
