package model.vo;

import model.data_structures.ILista;

public class VOAgnoPelicula implements Comparable <VOAgnoPelicula> {

	private int agno;
	private ILista<VOPelicula> peliculas;
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}
	@Override
	public int compareTo(VOAgnoPelicula arg0) {
		return agno-arg0.getAgno();
	}
	
	
}
