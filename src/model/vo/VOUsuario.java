package model.vo;

public class VOUsuario implements Comparable <VOUsuario> {

	private long idUsuario;	
	private long primerTimestamp = Long.MAX_VALUE;
	private int numRatings = 0;
	private double diferenciaOpinion= 0.0;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario arg0) {
		int res = 0;
		res = (int) (primerTimestamp - arg0.getPrimerTimestamp()); //El mas antiguo
		if (res == 0)res = arg0.getNumRatings()-numRatings; //El que mayor numero de ratings tenga
		if(res==0)res = (int) (idUsuario - arg0.getIdUsuario());
		return res;
	}
	
}
