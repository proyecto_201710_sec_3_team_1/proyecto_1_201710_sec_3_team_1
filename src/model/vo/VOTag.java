package model.vo;

public class VOTag implements Comparable <VOTag> {
	private String tag;
	private long timestamp;
	private long userId;
	private long movieId;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getUserId(){
		return userId;
	}
	public void setUserId(long userId){
		this.userId= userId;
	}
	public long getMovieId(){
		return userId;
	}
	public void setMovieId(long movieId){
		this.movieId= movieId;
	}
	@Override
	public int compareTo(VOTag arg0) {
		return (int) (timestamp-arg0.getTimestamp());
	}
	
}
