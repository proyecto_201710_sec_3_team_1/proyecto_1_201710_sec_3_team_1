package model.vo;

public class VOUsuarioConteo implements Comparable<VOUsuarioConteo>{
	private long idUsuario;
	private int conteo = 0;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	@Override
	public int compareTo(VOUsuarioConteo arg0) {
		return arg0.getConteo()-conteo;
	}
	
}
