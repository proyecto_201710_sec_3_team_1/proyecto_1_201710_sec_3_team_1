package model.vo;

public class VORecomRating {
	private long movieId;
	private double rating;
	
	public void setMovieId(long movieId){
		this.movieId=movieId;
	}
	public long getMovieId(){
		return movieId;
	}
	public void setRating(double rating){
		this.rating=rating;
	}
	public double getRating(){
		return rating;
	}
}
