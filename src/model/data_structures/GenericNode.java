package model.data_structures;

public class GenericNode<T> {

	private GenericNode<T> siguiente;
	private GenericNode<T> anterior;

	private T item;

	public GenericNode(){
		siguiente = null;
		anterior = null;
		item = null;
	}

	public GenericNode<T> darSiguiente(){
		return siguiente;
	}

	public GenericNode<T> darAnterior(){
		return anterior;
	}

	public void setSiguiente (GenericNode<T> pSig){
		siguiente = pSig;
	}

	public void setAnterior (GenericNode<T> pAnt){
		anterior = pAnt;
	}

	public void setItem(T pItem){
		item = pItem;
	}

	public T darItem(){
		return item;
	}
}
