package model.data_structures;

public class Queue<T> {

	private GenericNode<T> primero;

	private GenericNode<T> ultimo;

	public Queue(){
		primero =null;
		ultimo = null;
	}

	public void enQueue(T item){

		//Caso lista vacia
		if (primero == null){
			primero = new GenericNode<T>();
			primero.setItem(item);			
			ultimo = primero;			
		}
		//Caso no vacia
		else {
			GenericNode<T> add = new GenericNode<>();
			add.setItem(item);
			ultimo.setSiguiente(add);
			ultimo = add;
		}
	}

	public T deQueue(){
		T res = null;
		if(!isEmpty()){
			res = primero.darItem();
			primero = primero.darSiguiente();
		}

		return res;
	}

	public boolean isEmpty(){
		if (primero == null) return true;
		else return false;
	}

	public int size(){
		int res = 0;

		GenericNode<T> nodo = primero;

		while(nodo!=null){
			res++;
			nodo= nodo.darSiguiente();
		}
		return res;
	}
}
