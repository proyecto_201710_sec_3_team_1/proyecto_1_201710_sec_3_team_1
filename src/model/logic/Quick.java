package model.logic;


public class Quick<T extends Comparable<T>> {

	private T[] values;
	private int length;
	
	public void sort(T[] a)
	{
		 if(a == null || a.length == 0)
		 {
			 return;
		 }
		 values = a;
		 length = a.length;
		 quickSort(0, length-1);
	}
	
	
	private void quickSort(int izquierda, int derecha)
	{
		int i = izquierda;
		int j = derecha;
		T pivot = values[izquierda];
		while(i<= j)
		{
			while(values[i].compareTo(pivot) < 0)
			{
				i++;
			}
			while(values[j].compareTo(pivot) > 0)
			{				
				j--;
			}
			if (i <= j)
			{
				exchangeItems(i, j);
				i++;
				j--;
			}
		}
		if(izquierda < j)
			quickSort(izquierda, j);
		if(i < derecha)
			quickSort(i, derecha);
				
	}
	
	private void exchangeItems(int i, int j)
	{
		T aux = values[i];
		values[i] = values[j];
		values[j] = aux;
	}
	
}
