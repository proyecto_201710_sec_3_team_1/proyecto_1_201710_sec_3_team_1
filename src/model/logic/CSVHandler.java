package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import model.data_structures.ListaDobleEncadenada;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOTag;

public class CSVHandler {
	private ListaDobleEncadenada<VOPelicula> listaPeliculas;
	private ListaDobleEncadenada<VORating> listaRatings;
	private ListaDobleEncadenada<VOTag> listaTags;
	
	
	/**
	 * Este metodo lee y procesa los archivos csv (Aun no esta completamente implementado, de momento solo crea listas distintas segun el tipo de csv)
	 * @param pArchivo
	 * @throws Exception
	 */
	public ListaDobleEncadenada processFile (String pArchivo) throws Exception{
		File archivo = new File(pArchivo);
		FileReader fr = new FileReader(archivo);
		BufferedReader br = new BufferedReader(fr);
		
		String linea = br.readLine();
		
		if (linea.contains("rating")){
			linea = br.readLine();
			listaRatings = new ListaDobleEncadenada<VORating>();
			while(linea!=null){
				String[] datos = linea.split(",");
				VORating add = new VORating();
				add.setIdUsuario(Long.parseLong(datos[0]));
				add.setIdPelicula(Long.parseLong(datos[1]));
				add.setRating(Double.parseDouble(datos[2]));
				listaRatings.agregarElementoFinal(add);
				linea = br.readLine();
			}
			System.out.println("Lista de ratings creada.");

			br.close();
			fr.close();
			return listaRatings;
		}
		//Procesamiento de archivo "Tags.csv"
		else if(linea.contains("tag")){
			linea = br.readLine();
			listaTags = new ListaDobleEncadenada<VOTag>();
			while (linea!= null){
				Long userID = Long.parseLong(linea.substring(0, linea.indexOf(',')));
				linea = linea.substring(linea.indexOf(',')+1, linea.length());
				Long movieID = Long.parseLong(linea.substring(0, linea.indexOf(',')));
				
				linea = linea.substring(linea.indexOf(','), linea.length());
				String tag = linea.substring(0,linea.lastIndexOf(','));
				
				Long tstamp = Long.parseLong(linea.substring(linea.lastIndexOf(',')+1,linea.length()));
				
				VOTag add = new VOTag();
				add.setTag(tag);
				add.setTimestamp(tstamp);
				
				listaTags.agregarElementoFinal(add);
				linea = br.readLine();
			}

			br.close();
			fr.close();
			System.out.println("Lista de tags creada");
			return listaTags;
		}
		//Procesamiento de archivo "Movies.csv"
		else if(linea.contains("genres")){
			linea = br.readLine();
			listaPeliculas = new ListaDobleEncadenada<VOPelicula>();
			while( linea != null){
				Long movieId = Long.parseLong(linea.substring(0, linea.indexOf(',')));
				linea = linea.substring(linea.indexOf(',')+1,linea.length());
				
				String generos = linea.substring(linea.lastIndexOf(',')+1,linea.length());
				linea = linea.substring(0,linea.lastIndexOf(','));
				
				//Inicio procesamiento de titulo y agno
				int agno = -1;
				String titulo = linea;
				if(linea.endsWith(")")&& Character.isDigit(linea.charAt(linea.length()-2))||linea.endsWith(")"+"\"")&& Character.isDigit(linea.charAt(linea.length()-3))){
					//System.out.println(linea.substring(linea.lastIndexOf(')')-4,linea.lastIndexOf(')')));
					agno = Integer.parseInt(linea.substring(linea.lastIndexOf(')')-4,linea.lastIndexOf(')')));
					titulo = linea.substring(0, linea.lastIndexOf(' '));
					if(titulo.startsWith("\"")) titulo = titulo+"\"";
				}
				//Fin procesamiento de titulo y agno
				
				ListaDobleEncadenada<String> generosAsociados = new ListaDobleEncadenada<String>();
				String[] aGeneros = generos.split("|");
				int i = 0;
				while(i<aGeneros.length){
					generosAsociados.agregarElementoFinal(aGeneros[i]);
					i++;
				}
				
				VOPelicula add = new VOPelicula();
				
				add.setAgnoPublicacion(agno);
				add.setGenerosAsociados(generosAsociados);
				add.setTitulo(titulo);
				listaPeliculas.agregarElementoFinal(add);
				linea = br.readLine();
			}

			br.close();
			fr.close();
			System.out.println("lista peliculas creada");
			return listaPeliculas;
		}else{
			br.close();
			fr.close();
			throw new Exception ("CSV invalido");
		}
		
	}
	
	
}
