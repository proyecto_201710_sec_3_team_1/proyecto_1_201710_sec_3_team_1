package model.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class GenerarRecom {
	public static void generarArchivo(int n, String pRuta){
		if(!pRuta.endsWith("/")&&!pRuta.endsWith(".csv"))pRuta= pRuta+="/recomendacion.csv";
		else if(pRuta.endsWith("/")) pRuta +="recomendacion.csv";
		File archivo = new File(pRuta);
		try {			
			if(!archivo.exists()){
				archivo.createNewFile();
			}
			PrintWriter pw = new PrintWriter(archivo);
			pw.println("movieId,rating");
			for(int i = 0; i<n;i++){
				Random r = new Random();
				int movieId= r.nextInt(164980-1)+1;
				double rating= 0.5*(r.nextInt(11));
				pw.println(movieId+","+rating);
			}
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
