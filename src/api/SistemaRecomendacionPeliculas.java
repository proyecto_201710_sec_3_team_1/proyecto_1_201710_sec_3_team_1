package api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Comparator;
import java.util.Iterator;

import com.sun.org.apache.bcel.internal.generic.NEW;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queue;
import model.vo.VOAgnoPelicula;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VORecomRating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import model.logic.Quick;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{

	private ListaDobleEncadenada<VOTag> listaTags;
	private ListaDobleEncadenada<VOPelicula> listaPeliculas;
	private ListaDobleEncadenada<VORating> listaRatings;
	private ListaDobleEncadenada<VOUsuario> listaUsuarios;
	private ListaDobleEncadenada<VOGeneroPelicula> listaGeneros;

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		try{
			FileReader reader = new FileReader(new File(rutaPeliculas));
			BufferedReader buffer = new BufferedReader(reader);
			String cadena = buffer.readLine();
			cadena = buffer.readLine();
			while(cadena != null)
			{
				ListaDobleEncadenada<String> listaGeneros = new ListaDobleEncadenada<String>();
				String nombrePelicula = null;
				int agnoPelicula = 0;
				String generoPelicula = cadena.substring(cadena.lastIndexOf(",")+1);
				String aux = cadena.substring(0, cadena.lastIndexOf(","));
				int userId = Integer.parseInt(aux.split(",")[0]);

				if(generoPelicula.contains("\""))
				{
					generoPelicula = generoPelicula.replaceAll("\"", "");
				}

				String generosPelicula[] = generoPelicula.split("\\|"); 
				for (int i = 0; i<generosPelicula.length; i++)
				{
					listaGeneros.agregarElementoFinal(generosPelicula[i]);
				}

				String agno = "";
				if(aux.contains("("))
				{
					agno = aux.substring(aux.lastIndexOf("(") + 1, aux.lastIndexOf(")"));
					aux = aux.substring(0, aux.lastIndexOf(")"));
				}
				if(aux.contains("\""))
				{
					nombrePelicula = aux.replaceAll("\"", "");			
				}
				else
				{
					nombrePelicula = aux;
				}			
				if( !agno.isEmpty() )
				{
					agnoPelicula = Integer.parseInt(agno.substring( 0 , 4 ));
				}

				VOPelicula peliculaNueva = new VOPelicula();
				peliculaNueva.setIdUsuario(userId);
				peliculaNueva.setTitulo(nombrePelicula);
				peliculaNueva.setGenerosAsociados(listaGeneros);
				peliculaNueva.setAgnoPublicacion(agnoPelicula);

				listaPeliculas.agregarElementoFinal(peliculaNueva);			

				cadena = buffer.readLine();
			}

			reader.close();
			buffer.close();
			return true;

		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
			return false;
		}
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		try 
		{
			FileReader reader = new FileReader(new File(rutaRatings));
			BufferedReader buffer = new BufferedReader(reader);
			String cadena = buffer.readLine();
			cadena = buffer.readLine();
			while(cadena != null)
			{
				String[] ratingsList = cadena.split(",");
				long userId = Long.parseLong(ratingsList[0]);
				long movieId = Long.parseLong(ratingsList[1]);
				double ratingPelicula = Double.parseDouble(ratingsList[2]);
				long timeStamp = Long.parseLong(ratingsList[3]);

				VORating rating = new VORating();
				rating.setIdUsuario(userId);
				rating.setIdPelicula(movieId);
				rating.setRating(ratingPelicula);
				rating.setTimestamp(timeStamp);
				double caAcumulada = listaPeliculas.darElemento((int)movieId-1).getPromedioRatings()*listaPeliculas.darElemento((int)movieId-1).getNumeroRatings();
				listaPeliculas.darElemento((int)movieId-1).setNumeroRatings(listaPeliculas.darElemento((int)movieId-1).getNumeroRatings()+1);
				caAcumulada = (caAcumulada+ratingPelicula)/listaPeliculas.darElemento((int)movieId-1).getNumeroRatings();
				listaPeliculas.darElemento((int)movieId-1).setPromedioRatings(caAcumulada);
				this.listaRatings.agregarElementoFinal(rating);

				cadena = buffer.readLine();
			}
			reader.close();
			buffer.close();
			return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
			return false;
		}
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		try{

			FileReader reader = new FileReader(new File(rutaTags));
			BufferedReader buffer = new BufferedReader(reader);
			String cadena = buffer.readLine();
			cadena = buffer.readLine();
			while(cadena != null)
			{
				long userId = 0;
				long movieId = 0;
				String tag = null;
				long timeStamp = 0;

				if(cadena.contains("\"")) 
				{
					userId = Long.parseLong(cadena.substring(0, cadena.indexOf(",")));
					movieId = Long.parseLong(cadena.substring(cadena.indexOf(",") + 1, cadena.indexOf("\"") - 1));
					tag = cadena.substring(cadena.indexOf("\""), cadena.indexOf("\"") );
					timeStamp = Long.parseLong(cadena.substring(cadena.lastIndexOf(",") + 1 , cadena.length()));
				}
				else
				{
					String [] tagsList = cadena.split(",");
					userId = Long.parseLong(tagsList[0]);
					movieId = Long.parseLong(tagsList[1]);
					tag = tagsList[2];
					timeStamp = Long.parseLong(tagsList[3]);
				}
				if(tag.contains("\""))
				{
					tag = tag.replaceAll("\"", "");
				}

				VOTag tagNew = new VOTag();
				tagNew.setTag(tag);
				tagNew.setTimestamp(timeStamp);
				tagNew.setMovieId(movieId);
				tagNew.setUserId(userId);
				this.listaTags.agregarElementoFinal(tagNew);
				listaPeliculas.darElemento((int) movieId -1).setNumeroTags(listaPeliculas.darElemento((int) movieId-1).getNumeroTags()+1);
				listaPeliculas.darElemento((int) movieId-1).getTagsAsociados().agregarElementoFinal(tag);
				cadena = buffer.readLine();
			}

			reader.close();
			buffer.close();
			return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
			return false;
		}
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return listaPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return listaUsuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return listaTags.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		
		
		ListaDobleEncadenada<VOGeneroPelicula> res = new ListaDobleEncadenada<>();
		VOPelicula[] arreglopeliculas = new VOPelicula[listaPeliculas.darNumeroElementos()];

		Iterator<VOPelicula> iterPeliculas = listaPeliculas.iterator();
		int posicionPelicula = 0;
		while(iterPeliculas.hasNext())
		{
			VOPelicula pelicula = iterPeliculas.next();
			posicionPelicula ++;
			arreglopeliculas[posicionPelicula] = pelicula;
		}
		
		Quick<VOPelicula> sorter = new  Quick<VOPelicula>();
		sorter.sort(arreglopeliculas);
		
		for(int i =0; i<arreglopeliculas.length;i++){
			
			ILista<String> generosAsociados = arreglopeliculas[i].getGenerosAsociados();
			Iterator<String> IterGenAso = generosAsociados.iterator();
			while(IterGenAso.hasNext()){
				
				boolean Generolleno = false;
				boolean ExisteGenero = false;
				VOGeneroPelicula GeneroActual;
				VOGeneroPelicula GeneroEncontrado = new VOGeneroPelicula();
				
				String genero = IterGenAso.next();
				Iterator<VOGeneroPelicula> Interres = res.iterator();
				
				while(Interres.hasNext())
				{
					VOGeneroPelicula elemento = Interres.next();
					if(elemento.getGenero().equals(genero))
					{
					  ExisteGenero = true;
					  GeneroEncontrado = elemento;
					}
					
				}
				
				if (ExisteGenero == true)
				{
					GeneroActual = GeneroEncontrado;
					if (GeneroActual.getPeliculas().darNumeroElementos() > n);
					{
						Generolleno = true;
					}
				}
				else
				{
					ListaDobleEncadenada<VOPelicula> peliculas = new ListaDobleEncadenada<>();
					VOGeneroPelicula NuevoGenero = new VOGeneroPelicula();
					NuevoGenero.setGenero(genero);
					NuevoGenero.setPeliculas(peliculas);
					GeneroActual = NuevoGenero;
					res.agregarElementoFinal(NuevoGenero);
				}
				
				if (Generolleno==false)
				{
					GeneroActual.getPeliculas().agregarElementoFinal(arreglopeliculas[i]);
				}	
			}
				
		}
		
		return res;

	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		ListaDobleEncadenada<VOPelicula> res = new ListaDobleEncadenada<VOPelicula>();
		
		VOPelicula[] arreglopeliculas = new VOPelicula[listaPeliculas.darNumeroElementos()];

		Iterator<VOPelicula> iterPeliculas = listaPeliculas.iterator();
		int posicionPelicula = 0;
		while(iterPeliculas.hasNext())
		{
			VOPelicula pelicula = iterPeliculas.next();
			posicionPelicula ++;
			arreglopeliculas[posicionPelicula] = pelicula;
		}
		
		Quick<VOPelicula> sorter = new  Quick<VOPelicula>();
		sorter.sort(arreglopeliculas);
		
		for(int i =0; i<arreglopeliculas.length;i++)
		{
			ListaDobleEncadenada<Integer> listaAgnos = new ListaDobleEncadenada<>();
			
		}
		
		
		return res;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		
		ListaDobleEncadenada<VOGeneroPelicula> listaPeliculasRes = new ListaDobleEncadenada<VOGeneroPelicula>();

		VOPelicula[] arreglopeliculas = new VOPelicula[listaPeliculas.darNumeroElementos()];

		Iterator<VOPelicula> iterPeliculas = listaPeliculas.iterator();
		int posicionPelicula = 0;
		while(iterPeliculas.hasNext())
		{
			VOPelicula pelicula = iterPeliculas.next();
			posicionPelicula ++;
			arreglopeliculas[posicionPelicula] = pelicula;
		}
		
		for(int i = 0; i<listaGeneros.darNumeroElementos(); i++)
		{
			Quick<VOPelicula> sorter = new Quick<>();
			sorter.sort(arreglopeliculas);
		}
		
		for(int i = 0; i< listaGeneros.darNumeroElementos(); i++) 
		{
			listaGeneros.agregarElementoFinal(listaGeneros.darElemento(i));
		}
		
		return listaPeliculasRes;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		//Parte 1B
		ListaDobleEncadenada<VOGeneroUsuario> res = new ListaDobleEncadenada<VOGeneroUsuario>();

		//Generar arreglo ordenado de usuarios mas activos
		Iterator<VORating> iterRat = listaRatings.iterator();
		int numeroUsuarios = (int) listaRatings.darElemento(listaRatings.darNumeroElementos()-1).getIdUsuario();
		VOUsuarioConteo[] arreglo = new VOUsuarioConteo[numeroUsuarios];
		while(iterRat.hasNext()){
			VORating rating = iterRat.next();
			int usuario = (int) rating.getIdUsuario()-1;
			int conteo = arreglo[usuario].getConteo()+1;
			arreglo[usuario].setIdUsuario(rating.getIdUsuario());
			arreglo[usuario].setConteo(conteo);
		}
		Quick<VOUsuarioConteo> sorter = new Quick<VOUsuarioConteo>();
		sorter.sort(arreglo);
		//Generar lista respuesta
		int i = 0;
		while(i<arreglo.length){
			VOUsuarioConteo usr = arreglo[i];
			long id = usr.getIdUsuario();
			Iterator<VORating> iterRat2 = listaRatings.iterator();
			while (iterRat2.hasNext()){
				VORating rating = iterRat2.next();
				if (rating.getIdUsuario()==id){
					long movieId = rating.getIdPelicula()-1;
					VOPelicula pelicula = listaPeliculas.darElemento((int) movieId);
					Iterator<String> iterGeneros = pelicula.getGenerosAsociados().iterator();
					while(iterGeneros.hasNext()){
						String genero = iterGeneros.next();
						Iterator<VOGeneroUsuario> iterRes = res.iterator();
						boolean existe = false;
						VOGeneroUsuario a = null;
						while(iterRes.hasNext()&& !existe){
							a = iterRes.next();
							if (a.getGenero().equals(genero)){
								existe = true;
								iterRes.remove();
							}
						}
						if(existe){
							ListaDobleEncadenada<VOUsuarioConteo> listaExistente = (ListaDobleEncadenada<VOUsuarioConteo>) a.getUsuarios();
							if(listaExistente.darNumeroElementos()<n){
								listaExistente.agregarElementoFinal(usr);
								a.setUsuarios(listaExistente);
							}
							res.agregarElementoFinal(a);
						}else{
							VOGeneroUsuario add = new VOGeneroUsuario();
							add.setGenero(genero);
							ListaDobleEncadenada<VOUsuarioConteo> addLista = new ListaDobleEncadenada<VOUsuarioConteo>();
							addLista.agregarElementoFinal(usr);
							add.setUsuarios(addLista);
							res.agregarElementoFinal(add);
						}
					}
				}
			}
			i++;
		}	
		return res;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		ListaDobleEncadenada<VOUsuario> res = new ListaDobleEncadenada<>();
		VOUsuario[] arregloUsuarios = new VOUsuario[(int) listaRatings.darElemento(listaRatings.darNumeroElementos()-1).getIdUsuario()];
		Iterator<VORating> iter = listaRatings.iterator();
		while(iter.hasNext()){
			VORating rating = iter.next();
			int posUsuario = (int) (rating.getIdUsuario()-1);
			VOUsuario usr = arregloUsuarios[posUsuario];
			usr.setIdUsuario(rating.getIdUsuario());
			double oldDiff= usr.getDiferenciaOpinion()*usr.getNumRatings();
			oldDiff+=rating.getRating();
			usr.setNumRatings(usr.getNumRatings()+1);
			usr.setDiferenciaOpinion(oldDiff/usr.getNumRatings());
			if(usr.getPrimerTimestamp()== Long.MAX_VALUE || usr.getPrimerTimestamp()<rating.getTimestamp()){
				usr.setPrimerTimestamp(rating.getTimestamp());
			}
			arregloUsuarios[posUsuario]=usr;
		}
		Quick<VOUsuario> sorter = new Quick<VOUsuario>();
		sorter.sort(arregloUsuarios);
		for (int i = 0; i<arregloUsuarios.length;i++){
			res.agregarElementoFinal(arregloUsuarios[i]);
		}
		return res;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		ListaDobleEncadenada<VOGeneroPelicula> res = new ListaDobleEncadenada<>();
		Iterator<VOPelicula> iterPeliculas = listaPeliculas.iterator();
		while(iterPeliculas.hasNext()){
			VOPelicula pelicula = iterPeliculas.next();
			Iterator<String> iterGeneros = pelicula.getGenerosAsociados().iterator();
			while(iterGeneros.hasNext()){
				String genero = iterGeneros.next();
				Iterator<VOGeneroPelicula> iterRes = res.iterator();
				while(iterRes.hasNext()){
					VOGeneroPelicula elemento = iterRes.next();
					if(elemento.getGenero().equals(genero)){
						iterRes.remove();
						ListaDobleEncadenada<VOPelicula> listaPeliculasGenero = (ListaDobleEncadenada<VOPelicula>) elemento.getPeliculas();
						Iterator<VOPelicula> iterPeliculasGenero = listaPeliculasGenero.iterator();
						if(listaPeliculasGenero.darNumeroElementos()<n){
							listaPeliculasGenero.agregarElementoFinal(pelicula);
						}else{						
							boolean agregado = false;
							while(iterPeliculasGenero.hasNext()&&!agregado){
								VOPelicula peliculaRes = iterPeliculasGenero.next();
								if(peliculaRes.getNumeroTags()<pelicula.getNumeroTags()){
									iterPeliculasGenero.remove();
									listaPeliculasGenero.agregarElementoFinal(pelicula);
									agregado= true;
								}
							}
						}
						elemento.setPeliculas(listaPeliculasGenero);
						res.agregarElementoFinal(elemento);
					}
				}
			}
		}

		return res;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		//INICIO - crear arreglo de peliculas ordenado por a�o de publicacion
		ListaDobleEncadenada<VOAgnoPelicula> listaAgno = new ListaDobleEncadenada<>();
		Iterator<VOPelicula> iterPeliculas = listaPeliculas.iterator();
		while(iterPeliculas.hasNext()){
			VOPelicula pelicula = iterPeliculas.next();
			Iterator<VOAgnoPelicula> iterAgno = listaAgno.iterator();
			boolean existe = false;
			while (iterAgno.hasNext()&& !existe){
				VOAgnoPelicula agno = iterAgno.next();
				if (agno.getAgno() == pelicula.getAgnoPublicacion()){
					iterAgno.remove();
					ListaDobleEncadenada<VOPelicula> listaActualAgno = (ListaDobleEncadenada<VOPelicula>) agno.getPeliculas();
					listaActualAgno.agregarElementoFinal(pelicula);
					agno.setPeliculas(listaActualAgno);
					listaAgno.agregarElementoFinal(agno);
					existe =true;
				}
			}
			if (!existe){
				VOAgnoPelicula add = new VOAgnoPelicula();
				add.setAgno(pelicula.getAgnoPublicacion());
				ListaDobleEncadenada<VOPelicula> addPelicula = new ListaDobleEncadenada<>();
				addPelicula.agregarElementoFinal(pelicula);
				add.setPeliculas(addPelicula);
				listaAgno.agregarElementoFinal(add);
			}
		}
		VOAgnoPelicula[] arregloVOAgno = new VOAgnoPelicula[listaAgno.darNumeroElementos()];
		Iterator<VOAgnoPelicula> iterVOAgno = listaAgno.iterator();
		for(int i = 0 ; i<listaAgno.darNumeroElementos()&&iterVOAgno.hasNext();i++){
			arregloVOAgno[i]=iterVOAgno.next();
		}
		Quick<VOAgnoPelicula> sorter = new Quick<VOAgnoPelicula>();
		sorter.sort(arregloVOAgno);
		//FIN- creacion de arreglo de peliculas ordenado por a�o de publicacion
		//INICIO- crear lista del requerimiento
		ListaDobleEncadenada<VOUsuarioGenero> listaVOUG = new ListaDobleEncadenada<>();
		for(int i =0; i<arregloVOAgno.length;i++){
			VOAgnoPelicula agno = arregloVOAgno[i];
			Iterator<VOPelicula> iterAgnoPeliculas =agno.getPeliculas().iterator();
			//Iterar las peliculas de cada agno
			while (iterAgnoPeliculas.hasNext()){
				VOPelicula pelicula = iterAgnoPeliculas.next();
				Iterator<String> iterTagsPelicula = pelicula.getTagsAsociados().iterator();
				//Iterar cada tag de la pelicula y agregarlo al usuario por genero
				while(iterTagsPelicula.hasNext()){
					String tag = iterTagsPelicula.next();
					Iterator<VOTag> iterDatosTags = listaTags.iterator();
					VOTag elemento=null;
					boolean encontrado = false;
					//Buscar el tag en la base de datos listaTags para obtener mas informacion
					while(iterDatosTags.hasNext()&&!encontrado){
						VOTag temp = iterDatosTags.next();
						if(temp.getTag().equals(tag)){
							elemento = temp;
							encontrado = true;
						}
					}
					Iterator<VOUsuarioGenero> iterVOUG = listaVOUG.iterator();
					boolean existe = false;
					while(iterVOUG.hasNext()&&!existe){
						VOUsuarioGenero voug = iterVOUG.next();
						if(voug.getIdUsuario()==elemento.getUserId()){
							iterVOUG.remove();
							ListaDobleEncadenada<VOGeneroTag> nLista = (ListaDobleEncadenada<VOGeneroTag>) voug.getListaGeneroTags();
							Iterator<String> iterGenerosPelicula = pelicula.getGenerosAsociados().iterator();
							while(iterGenerosPelicula.hasNext()){ //iterar cada genero asociado a la pelicula
								String genero = iterGenerosPelicula.next();
								Iterator<VOGeneroTag> iterGeneroTag = nLista.iterator();
								boolean existeGenero = false;
								//I- Agrega el tag a los generos asociados a la pelicula.
								while(iterGeneroTag.hasNext()&&!existeGenero){
									VOGeneroTag generoTag = iterGeneroTag.next();
									if (generoTag.getGenero().equals(genero)){
										iterGeneroTag.remove();
										ListaDobleEncadenada<String> listaTG = (ListaDobleEncadenada<String>) generoTag.getTags();
										listaTG.agregarElementoFinal(tag);
										generoTag.setTags(listaTG);
										nLista.agregarElementoFinal(generoTag);
										existeGenero=true;
									}
								}
								if(!existeGenero){
									VOGeneroTag generoTag = new VOGeneroTag();
									ListaDobleEncadenada<String> listaTG = new ListaDobleEncadenada<>();
									listaTG.agregarElementoFinal(tag);
									generoTag.setTags(listaTG);
									generoTag.setGenero(genero);
									nLista.agregarElementoFinal(generoTag);									
								}
								//F- Agrega el tag a los generos asociados a la pelicula.
							}
							listaVOUG.agregarElementoFinal(voug);
							existe = true;	
						}
					}
					if (!existe){
						VOUsuarioGenero voug = new VOUsuarioGenero();
						voug.setIdUsuario(elemento.getUserId());
						ListaDobleEncadenada<VOGeneroTag> listaGeneroTagNueva = new ListaDobleEncadenada<>();
						Iterator<String> iterGenerosPelicula = pelicula.getGenerosAsociados().iterator();
						while(iterGenerosPelicula.hasNext()){
							String genero = iterGenerosPelicula.next();
							VOGeneroTag add = new VOGeneroTag();
							add.setGenero(genero);
							ListaDobleEncadenada<String>listaTagsNueva = new ListaDobleEncadenada<>();
							listaTagsNueva.agregarElementoFinal(tag);
							add.setTags(listaTagsNueva);
							listaGeneroTagNueva.agregarElementoFinal(add);
						}
					}
				}
			}
		}
		//FIN - crear lista del requerimiento
		//INICIO- Ordenar tags por orden alfabetico
		Iterator<VOUsuarioGenero> iterVOUG = listaVOUG.iterator();
		while(iterVOUG.hasNext()){
			VOUsuarioGenero voug = iterVOUG.next();
			Iterator<VOGeneroTag> iterGT = voug.getListaGeneroTags().iterator();
			while(iterGT.hasNext()){
				VOGeneroTag vogt = iterGT.next();
				String[] tags = new String[vogt.getTags().darNumeroElementos()];
				Iterator<String> iterTags =vogt.getTags().iterator();
				int i = 0;
				while(iterTags.hasNext()){
					String tag = iterTags.next();
					tags[i]=tag;
					i++;
				}
				Quick<String> sorterTags= new Quick<String>();
				sorterTags.sort(tags);
				ListaDobleEncadenada<String>listaOrdenada= new ListaDobleEncadenada<>();
				for(int j = 0; j<tags.length;j++) listaOrdenada.agregarElementoFinal(tags[j]);
				vogt.setTags(listaOrdenada);
			}
		}
		//FIN- Ordenar tags por orden alfabetico
		return listaVOUG;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		ListaDobleEncadenada<VOPeliculaUsuario> res = new ListaDobleEncadenada<>();
		Queue<VORecomRating> colaRecomendacion = new Queue<VORecomRating>();
		try{
			File archivo = new File(rutaRecomendacion);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			String linea=br.readLine();
			linea=br.readLine();
			while(linea!=null){
				String parametros[]=linea.split(",");
				VORecomRating vorr= new VORecomRating();
				long id = Long.parseLong(parametros[0]);
				double rating = Double.parseDouble(parametros[1]);
				vorr.setMovieId(id);
				vorr.setRating(rating);
				colaRecomendacion.enQueue(vorr);
				linea = br.readLine();
			}
			br.close();
			fr.close();
		}catch (Exception e){
			System.out.println("no se pudo leer el archivo recomendaciones");
		}
		while(!colaRecomendacion.isEmpty()){
			VORecomRating vorr = colaRecomendacion.deQueue();
			Iterator<VORating> iterRatings = listaRatings.iterator();
			while(iterRatings.hasNext()){
				VORating rating = iterRatings.next();
				if(vorr.getMovieId()==rating.getIdPelicula()){
					double temp = rating.getRating()-vorr.getRating();
					double dif = temp*temp;
					if (dif<=0.25){
						VOUsuario usuario = new VOUsuario();
						usuario.setDiferenciaOpinion(dif);
						usuario.setIdUsuario(rating.getIdUsuario());
						Iterator<VORating> iterRatings2 = listaRatings.iterator();
						while(iterRatings2.hasNext()){
							VORating ratingUsuario= iterRatings.next();
							if (rating.getIdUsuario()== ratingUsuario.getIdUsuario()){
								usuario.setNumRatings(usuario.getNumRatings()+1);
								if(usuario.getPrimerTimestamp()>ratingUsuario.getTimestamp())usuario.setPrimerTimestamp(ratingUsuario.getTimestamp());
							}
						}
						Iterator<VOPeliculaUsuario> iterRes = res.iterator();
						boolean existe = false;
						while (iterRes.hasNext()&&!existe){
							VOPeliculaUsuario pelicula = iterRes.next();
							if (pelicula.getIdPelicula()==vorr.getMovieId()){
								iterRes.remove();
								ListaDobleEncadenada<VOUsuario> listaUsuarios = (ListaDobleEncadenada<VOUsuario>) pelicula.getUsuariosRecomendados();
								listaUsuarios.agregarElementoFinal(usuario);
								pelicula.setUsuariosRecomendados(listaUsuarios);
								res.agregarElementoFinal(pelicula);
								existe = true;
							}
						}
						if(!existe){
							VOPeliculaUsuario pelicula = new VOPeliculaUsuario();
							pelicula.setIdPelicula(vorr.getMovieId());
							ListaDobleEncadenada<VOUsuario> usuariosRecomendados = new ListaDobleEncadenada<>();
							usuariosRecomendados.agregarElementoFinal(usuario);
							pelicula.setUsuariosRecomendados(usuariosRecomendados);
						}
					}
				}
			}
		}
		
		return res;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		ListaDobleEncadenada<VOTag> res = new ListaDobleEncadenada<>();
		VOPelicula pelicula=listaPeliculas.darElemento(idPelicula);
		Iterator<String> iterTags = pelicula.getTagsAsociados().iterator();
		while(iterTags.hasNext()){
			String tag = iterTags.next();
			Iterator<VOTag> iterListaTags = listaTags.iterator();
			VOTag tagAsociado = null;
			boolean encontrado = false;
			while (iterListaTags.hasNext()&&!encontrado){
				VOTag temp = iterListaTags.next();
				if (temp.getTag().equals(tag)){
					tagAsociado = temp;
					encontrado = true;
				}
			}
			res.agregarElementoFinal(tagAsociado);
		}
		return res;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}
	
	

}
